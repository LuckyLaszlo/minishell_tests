
**summary**

# exit status
  6 / 6
# redirections
  50 / 50
# builtins env
! 37 / 47
# heredocs
  1 / 1
# expansions
  248 / 248
# expansions var
! 110 / 120
# pipes
  40 / 40
# builtins
  40 / 40
# else
  29 / 29
# total : 276 / 366



**details**

# --------------------------------
# builtins env

1. `export ""` ! segfault !


# --------------------------------
# expansions var

1. `echo $"USER"` quotes inside $


