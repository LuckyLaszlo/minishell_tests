ulimit -n

cat Makefile | grep pr | head -n 5 | cd test (mybin)
echo $?

cat Makefile | grep pr | head -n 5 | cat test (bin)
echo $?

cat Makefile | grep pr | head -n 5 | hello (NA)
echo $?



#---------------------------------------------
# pwd dans un dossier qui n'existe pas ne s'affiche pas

mkdir dir
cd dir
rmdir ../dir
cd ./
pwd
echo $PWD
echo $OLDPWD
export PWD=
export OLDPWD=

mkdir dir
cd dir
rmdir ../dir
cd ./
cd ././././././//////////./././././././//////./
pwd
echo $PWD
echo $OLDPWD
export PWD=
export OLDPWD=

mkdir dir
cd dir
rmdir ../dir
cd ./
cd ../
pwd
echo $PWD
echo $OLDPWD
export PWD=
export OLDPWD=

mkdir dir
ln -s dir link
cd link
rmdir ../dir
cd ./
pwd
echo $PWD
echo $OLDPWD
export PWD=
export OLDPWD=

mkdir dir
ln -s dir link
cd link
rmdir ../dir
cd ./
cd ../
pwd
echo $PWD
echo $OLDPWD
export PWD=
export OLDPWD=


#---------------------------------------------
# export PWD et OLDPWD ne dependent pas des meme action que bash
# (dans bash : cd ne creer pas PWD, et OLDPWD n'existe que si PWD existe)

unset PWD
cd ..
env | grep PWD

unset OLDPWD
cd ..
env | grep OLDPWD

unset PWD
cd ..
export PWD=new_value
env | grep PWD


unset PWD
cd ..
export PWD
env | grep PWD


#---------------------------------------------
# export ne reconnait pas +=

export APPEND=1
export APPEND+=2
env | grep APPEND


#---------------------------------------------
# export seul n'affiche pas les variables d'env

export | grep -v -E '^declare -x _='
env

export 1 _+ _OK _2? _OK2=VAR =NG OK3=
export | grep -v -E '^declare -x _='
env

export dquote='"' dollar='$' backslash='\' backquote='`' newline='\n'
export | grep -v -E '^declare -x _='
env

export APPEND=1
export APPEND+=2
export | grep APPEND
env

export APPEND_NONE=1
export APPEND_NONE+=
export | grep APPEND_NONE
env

export APPEND_NONE=
export APPEND_NONE=
export APPEND_NONE+=
export | grep APPEND_NONE
env

export APPEND_EMPTY=""
export APPEND_EMPTY=""
export APPEND_EMPTY=""
export | grep APPEND_EMPTY
env

export OVERWRITE=42
export OVERWRITE=a
export | grep OVERWRITE
env

export A=B=C=D
export | grep A
env

export PATH
export | grep PATH
env

export PWD
export | grep PWD
env

export OLDPWD
export | grep OLDPWD
env

unset PWD
cd ..
export | grep PWD
env

unset OLDPWD
cd ..
export | grep OLDPWD
env

unset PWD
cd ..
export PWD=new_value
export | grep PWD
env

unset OLDPWD
cd ..
export OLDPWD=new_value
export | grep OLDPWD
env

unset PWD
cd ..
export PWD
export | grep PWD
env

unset OLDPWD
cd ..
export OLDPWD
export | grep OLDPWD
env

#---------------------------------------------
# cd /bin redirects to /usr/bin

cd /bin
pwd

cd /bin/../
pwd

cd /
pwd
cd /bin
pwd

cd /
pwd
cd //bin
pwd

cd //
pwd
cd //bin
pwd

cd ///
pwd
cd ///bin
pwd

cd ////
pwd
cd ////bin
pwd

cd /////
pwd
cd /////bin
pwd

cd //////
pwd
cd //////bin
pwd

cd ///////
pwd
cd ///////bin
pwd


#---------------------------------------------
# https://stackoverflow.com/questions/20690828/what-does-double-slash-in-cd-mean-in-linux
# pathname with two slashes

export HOME=//
cd
pwd

export HOME=//bin/..////////////bin/../
cd
pwd

#---------------------------------------------
# Race condition, unpredictable behaviour

echo DOET HET NIET > test1 | cat test1


#---------------------------------------------
# redirection stderr

echo hello > out1.txt > out2.txt > out3.txt 2> err1.txt 2> err2.txt 2> err3.txt
cat out1.txt out2.txt out3.txt err1.txt err2.txt err3.txt

echo hello >> out1.txt >> out2.txt >> out3.txt 2>> err1.txt 2>> err2.txt 2>> err3.txt
cat out1.txt out2.txt out3.txt err1.txt err2.txt err3.txt

ls > out1.txt > out2.txt > out3.txt 2> err1.txt 2> err2.txt 2> err3.txt
cat out1.txt out2.txt out3.txt err1.txt err2.txt err3.txt

ls >> out1.txt >> out2.txt >> out3.txt 2>> err1.txt 2>> err2.txt 2>> err3.txt
cat out1.txt out2.txt out3.txt err1.txt err2.txt err3.txt

ls no_such_file > out1.txt > out2.txt > out3.txt 2> err1.txt 2> err2.txt 2> err3.txt
cat out1.txt out2.txt out3.txt err1.txt err2.txt err3.txt

ls no_such_file >> out1.txt >> out2.txt >> out3.txt 2>> err1.txt 2>> err2.txt 2>> err3.txt
cat out1.txt out2.txt out3.txt err1.txt err2.txt err3.txt

./a.out 3> file3 2> file2 1> file1
cat file3 file2 file1
printf '#include<unistd.h>\nint main(){write(1, "fd1\\n", 4);write(2, "fd2\\n", 4);write(3, "fd3\\n", 4);}' > a.c && gcc a.c

./a.out 1> file1 2> file2 3> file3
cat file1 file2 file3
printf '#include<unistd.h>\nint main(){write(1, "fd1\\n", 4);write(2, "fd2\\n", 4);write(3, "fd3\\n", 4);}' > a.c && gcc a.c

echo a 1> 1.txt 2> 2.txt 3> 3.txt
./a.out 1>> 1.txt 2>> 2.txt 3>> 3.txt
cat 1.txt 2.txt 3.txt
printf '#include<unistd.h>\nint main(){write(1, "fd1\\n", 4);write(2, "fd2\\n", 4);write(3, "fd3\\n", 4);}' > a.c && gcc a.c

echo a 3> 3.txt 2> 2.txt 1> 1.txt 2> 2.txt 3> 3.txt
echo b 1>> 1.txt 2>> 2.txt 3>> 3.txt 2>> 2.txt 1>> 1.txt
./a.out 1>> 1.txt 2>> 2.txt 3>> 3.txt 2>> 2.txt 1>> 1.txt
cat 1.txt 2.txt 3.txt
printf '#include<unistd.h>\nint main(){write(1, "fd1\\n", 4);write(2, "fd2\\n", 4);write(3, "fd3\\n", 4);}' > a.c && gcc a.c



#----------------------------------------------
# ; Semicolon separator

echo hallo | echo poep >x1 >x2
echo hallo ; echo poep >x1 >x2

echo hallo | echo hoi > y1 >y2 ; cat -e
echo "hello | this is some stuff > touch this file" | grep more

echo bonjour ; |

echo bonjour |;

;

echo coucou | ;

echo ;;

exit 0 0; echo "this message should not be displayed" 2> file; ls

echo $PWD, mkdir dir; ln -s dir symdir; cd symdir

#----------------------------------------------
# Export builtins

export | grep -e "HOME="

export | grep -e "PWD="

export a="\\"

export

# var without export
POEP=directory1
MEERPOEP=directory2

export PATH="test:working:multiplepath:directory1:directory2"
env


#----------------------------------------------
# CD builtins

export CDPATH="/"
cd bin
pwd

export CDPATH="/"
cd
pwd

export CDPATH="/"
export HOME=
cd
pwd

export CDPATH="/"
unset HOME
cd
pwd

export CDPATH=""
mkdir -p a/b
cd b
pwd

export CDPATH="./"
mkdir dir
cd dir
pwd

export CDPATH="../a"
mkdir -p a/b/c
cd b
pwd
cd a
pwd
cd b
pwd

export CDPATH=a
mkdir -p a/b/c
cd b
pwd

export CDPATH=A
mkdir -p a/b/c
cd b
pwd

export CDPATH=a
mkdir -p a/b/c
cd ./b
pwd

export CDPATH=a
mkdir -p a/b/c
cd a
cd ../b
pwd

export CDPATH=a
mkdir -p a/b/c
cd .
pwd

export CDPATH=a
mkdir -p a/b/c
cd a
cd ..
pwd

export CDPATH=a
mkdir -p a/b/c
mkdir b
cd b
pwd

export CDPATH=a/b
mkdir -p a/b/c
cd c
pwd

export CDPATH=a:b:a:b
mkdir -p a/test  b/test
mkdir -p a:b/test
cd test
pwd

export CDPATH=b:a:a:b
mkdir -p a/test  b/test
mkdir -p a:b/test
cd test
pwd

export CDPATH=a:b:a:b
mkdir -p a/hello  b/hello
mkdir -p a:b/test
cd test
pwd

export CDPATH=a:b:a:b
mkdir -p a/test  b/test
mkdir -p a:b/test
chmod 000 a/test
cd test
pwd
cd ../../
chmod 777 a/test

export CDPATH="/"
cd /
pwd

export CDPATH="/"
cd /bin
pwd

export CDPATH="/bin"
cd /bin
pwd

export CDPATH="/bin"
cd /bin
pwd

export CDPATH="a"
mkdir a
cd a
pwd

export CDPATH="a"
mkdir -p a/a
cd a
pwd

export CDPATH=//
cd bin

export CDPATH=//bin/..////////////bin/../
cd bin

export CDPATH=///
cd bin

export CDPATH=":"
cd dir
pwd

export CDPATH="hello:"
cd dir
pwd

export CDPATH=":/"
cd dir
pwd

export CDPATH="::/"
cd bin
pwd

export CDPATH="::/:"
cd bin
pwd

export CDPATH=""
cd bin
pwd

export CDPATH="/:"
cd bin
pwd

export CDPATH="//::"
cd bin
pwd


#----------------------------------------------
# special characters

echo "$222"

echo "hallo$USER***$USER$2$"

echo "$53535"

echo $53535

echo "$5fsdlfjlfj"

echo $1


#----------------------------------------------
# expansions with quotes $""

echo $"USER"

echo $"etet"

echo $"hallo"

echo /$"123$USER"

echo hallo$"USER"

echo hallo$"POEP"

echo hallo$'USER'

echo $""

echo $"nothing"

echo $"$USER"


#----------------------------------------------
# ' " single quotes

echo hallo 'ma'ran' siemons

echo hallo 'ma\"ran' siemons

echo hallo 'ma\'ran'' siemons

echo "hallo"maran"

echo hallo"

echo poep"

echo poep"""

echo hallo"\poep

echo " " "

echo "

echo """

echo '

echo $USER"

echo "$USER

echo hallo$'USER' '

echo $USER'	

echo $USER'	

echo $'	

echo $'	' '

echo $"USERhallo"



#----------------------------------------------
# \ Backslash

echo "bip | bip ; coyotte > < \" "

echo bonjour > test\ 1
ls
rm test

echo bonjour \; ls

echo \"test\"

echo "123\"456"

echo poep\'

echo "poep\"

echo "hallo\"\poep"

#echo "\$"

#echo "\""

#echo \'1234'

#echo \hallo

#echo "123\$456"

#echo "123\\456"

#echo “$\hallo”

#echo $"\hallo"

#echo "\\hallo"

#echo "hallo\\hoi"

#echo "\\$USER"

#echo "123\"456"

#echo "hallo\"\poep"			

#echo hallo$\\abc

#echo hallo$\abc

#echo hallo$\abc

#echo '$USER'$USER\$USER/"$USER"

#echo \$?

#echo \"

#echo \poep

#echo "123\'456"

#echo 123\'456

#echo "'\$USER'"

#echo "\$USER"

#echo "123\"456"

#echo "poep\'"

#echo "poep\""

#echo "\$USER"

#echo \$USER

#echo \$?

#echo hallo\$USER

#echo ma\

#echo 'ma\'									Hij ziet de \ niet als escape, print gewoon \

#echo 'ma\'									Hij ziet de \ niet als escape, print gewoon \

#echo hallo ma\"ran siemons

#echo hallo 'ma\'ran' siemons

#echo    "\""

#echo   \'

#echo \n ' \n this is the; "first line"'

#echo 00\\\'pp

#echo \$HOME

#echo "\\s"

#echo "\n \\n \\\n"

#echo \>

#echo 00\\\\\'pp

#echo 00\\\\'pp'

echo   \\

echo "\\"

echo \poep

echo \1

echo 123\!456

echo 123\456

echo "\"

echo   \" "\\"    "\""   \\   \'   \"

echo "\\"

echo \"1234"

echo "1234\"

echo "1234\\"

echo "\\"

echo \a
echo \b
echo \c
echo \d
echo \e
echo \!
echo \"
echo \#
echo \$
echo \%
echo \&
echo \'
echo \(
echo \)
echo \*
echo \+
echo \,
echo \-
echo \.
echo \/
echo \0
echo \1
echo \2
echo \3
echo \4
echo \5
echo \6
echo \7
echo \8
echo \9
echo \:
echo \=
echo \?
echo \@
echo \A
echo \B
echo \C
echo \D
echo \E
echo \F
echo \G
echo \H
echo \I
echo \J
echo \K
echo \L
echo \M
echo \N
echo \O
echo \P
echo \Q
echo \R
echo \S
echo \T
echo \U
echo \V
echo \W
echo \X
echo \Y
echo \Z
echo \[
echo \\
echo \]
echo \^
echo \_
echo \a
echo \b
echo \c
echo \d
echo \e
echo \f
echo \g
echo \h
echo \i
echo \j
echo \k
echo \l
echo \m
echo \n
echo \o
echo \p
echo \q
echo \r
echo \s
echo \t
echo \u
echo \v
echo \w
echo \x
echo \y
echo \z
echo \{
echo \}
echo \~

echo \$

echo "\$"

export HELLO=hello
echo \$HELLO

export HELLO=hello
echo \\$HELLO

export HELLO=hello
echo "\$HELLO"

export HELLO=hello
echo "\\$HELLO"

export HELLO=hello
echo \\"$HELLO"

export HELLO=hello
echo "\"\"$HELLO"

echo \-n hello

echo \-\n \h\e\l\l\o

echo \$PATH

echo "\$PATH"

echo \"$\"

echo "\$\$USER\\$USER""$USER"

echo \$USER

echo \\$USER

echo \\\$USER

echo \\\\$USER

# PWD, differ from bash

# pwd symbolic link
mkdir dir
ln -s dir link
cd link
pwd

# pwd rmdir
mkdir dir
ln -s dir link
rmdir dir
cd link
pwd

mkdir dir
cd dir
rmdir ../dir
cd ./
pwd
cd .
pwd
cd ./././././
pwd
cd .////////./
pwd
cd ../
pwd

mkdir -p a/b/c
cd a/b/c
rm -rf ../../b
cd ./
pwd
cd ../
pwd
cd .////./
pwd

mkdir -p a/b/c
cd a/b/c
rm -rf ../../b
cd ./
pwd
cd .././.
pwd
cd ./
pwd
cd ../
pwd
