touch file_name_in_current_dir
file_name_in_current_dir

not_cmd

minishell

./minishell

l^Ds

touch testt
chmod 000 testt
ls
./testt

/bin/ls

/bin/pwd

ls -la

/bin/chmod

/bin/ls -la

ctrl-C

ctrl-D

ctrl-\

asdf ctrl-C

asdf ctrl-D

cat ctrl-C

asdf ctrl-\

cat ctrl-D

hoi

ls -la

ls

/bin/ls

/binn/ls

$"
$'
123"456

\1
123\!456
123\456

“$rewtwet”

\maran

hallo\hoi
\hallo
\hallo

/bin/ls
touch a
a

/bin/ls -a
touch a
a

/bin/ls | /usr/bin/grep a
touch aa ab ac

/bin/ls | /usr/bin/grep a | /usr/bin/grep c
touch aa ab ac

ls
touch a
a

ls -a
touch a
a

ls | grep a
touch aa ab ac

ls | grep a | grep c
touch aa ab ac

echo hello world | cat

no_such_file

./no_such_file

/bin/ls/no_such_file

/bin/no_such_dir/file

perm
touch perm
chmod 000 perm

./perm
touch perm
chmod 000 perm

perm
touch perm
chmod 100 perm

./perm
touch perm
chmod 100 perm

perm
touch perm
chmod 300 perm

./perm
touch perm
chmod 300 perm

perm
touch perm
chmod 500 perm

./perm
touch perm
chmod 500 perm

./empty
touch empty
chmod +x empty

dir
mkdir dir

./dir
mkdir dir

./exe
printf "#!/bin/bash\necho 42" > exe
chmod +x exe
ln -s exe sym
chmod -h -x sym

echo a | $NO_ENV | cat
unset NO_ENV
